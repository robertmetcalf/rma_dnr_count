﻿using System;
using System.Collections.Generic;
using System.Data;
using FLUSA.Data.Oracle;
using System.IO;
using System.Net.Mail;
using ClosedXML.Excel;
using System.Configuration;
using System.Text;

namespace RMA_DNR
{
    class Program
    {
        static void Main(string[] args)
        {
            //Get RMA DNRs

            {
                //Set up datatable to hold RMAs where the customer has claimed non-receipt
                DataTable resolutions = new DataTable("RmaDnr");
                resolutions.Columns.Add("OrdNum");
                resolutions.Columns.Add("OrdDte");
                resolutions.Columns.Add("CustName");
                resolutions.Columns.Add("CustEmail");
                resolutions.Columns.Add("CustPhone");
                resolutions.Columns.Add("RmaNum");
                resolutions.Columns.Add("RmaDte");
                resolutions.Columns.Add("RmaStat");
                resolutions.Columns.Add("NameCt");
                resolutions.Columns.Add("EmailCt");
                resolutions.Columns.Add("PhoneCt");


                //Query databases to find problem orders and resolutions

                using (OracleDB EOM = new OracleDB(OracleDB.DatabaseType.MANH))
                {
                    string eomTable = System.Configuration.ConfigurationManager.AppSettings["eomTable"];
                    StringBuilder sbCustInfo = new StringBuilder();
                    sbCustInfo
                      .Append("WITH BASE_DATA AS (select ORIG_ORD                                    ").Append(Environment.NewLine)
                      .Append(", ORIG_ORD_DTE                                                        ").Append(Environment.NewLine)
                      .Append(", CUST_NAME                                                           ").Append(Environment.NewLine)
                      .Append(", EMAIL                                                               ").Append(Environment.NewLine)
                      .Append(", PHONE                                                               ").Append(Environment.NewLine)
                      .Append(",  RMA_NUM                                                            ").Append(Environment.NewLine)
                      .Append(", RMA_CRTE_DTE                                                        ").Append(Environment.NewLine)
                      .Append(", RMA_STAT_NAME                                                       ").Append(Environment.NewLine)
                      .Append("from (                                                                ").Append(Environment.NewLine)
                      .Append("select distinct ORIG_ORD                                              ").Append(Environment.NewLine)
                      .Append(", ORIG_ORD_DTE                                                        ").Append(Environment.NewLine)
                      .Append(", CUST_NAME                                                           ").Append(Environment.NewLine)
                      .Append(", EMAIL                                                               ").Append(Environment.NewLine)
                      .Append(", PHONE                                                               ").Append(Environment.NewLine)
                      .Append(", po.tc_purchase_orders_id RMA_NUM                                    ").Append(Environment.NewLine)
                      .Append(", trunc(po.created_dttm) RMA_CRTE_DTE                                 ").Append(Environment.NewLine)
                      .Append(", rstat.rma_stat_name RMA_STAT_NAME                                   ").Append(Environment.NewLine)
                      .Append("from olm2012.pob_con_rma pcr                                          ").Append(Environment.NewLine)
                      .Append("join olm2012.purchase_orders po                                       ").Append(Environment.NewLine)
                      .Append("on po.purchase_orders_id = pcr.pob_con_rma_id                         ").Append(Environment.NewLine)
                      .Append("join olm2012.rma_stat rstat                                           ").Append(Environment.NewLine)
                      .Append("on pcr.rma_stat_id = rstat.rma_stat_id                                ").Append(Environment.NewLine)
                      .Append("join olm2012.purchase_orders_line_item poli                           ").Append(Environment.NewLine)
                      .Append("on po.purchase_orders_id = poli.purchase_orders_id                    ").Append(Environment.NewLine)
                      .Append("left outer join olm2012.pob_con_rma_ord_item pcroi                    ").Append(Environment.NewLine)
                      .Append("on pcroi.pob_con_rma_ord_item_id = poli.purchase_orders_line_item_id  ").Append(Environment.NewLine)
                      .Append("join olm2012.POB_RET_REAS prr                                         ").Append(Environment.NewLine)
                      .Append("on prr.pob_ret_reas_id = pcroi.pob_ret_reas_id                        ").Append(Environment.NewLine)
                      .Append("join OLM2012.POB_RET_ACTION pra                                       ").Append(Environment.NewLine)
                      .Append("on pra.pob_ret_action_id = pcroi.APPROVE_POB_RET_ACTION_ID            ").Append(Environment.NewLine)
                      .Append("  LEFT OUTER JOIN                                                     ").Append(Environment.NewLine)
                      .Append("  (select po.tc_purchase_orders_id ORIG_ORD                           ").Append(Environment.NewLine)
                      .Append("  , po.purchase_orders_id                                             ").Append(Environment.NewLine)
                      .Append("  , trunc(po.created_dttm) ORIG_ORD_DTE                               ").Append(Environment.NewLine)
                      .Append("  , upper(po.customer_firstname ||' '||po.customer_lastname) CUST_NAME").Append(Environment.NewLine)
                      .Append("  , replace(upper(po.customer_email),'.','') EMAIL                    ").Append(Environment.NewLine)
                      .Append("  , replace(po.customer_phone,'-','') PHONE                           ").Append(Environment.NewLine)
                      .Append("  from olm2012.purchase_orders po                                     ").Append(Environment.NewLine)
                      .Append("  ) orig_ord                                                          ").Append(Environment.NewLine)
                      .Append("on orig_ord.purchase_orders_id = po.parent_purchase_orders_id         ").Append(Environment.NewLine)
                      .Append("where pcroi.is_receivable = '0'                                       ").Append(Environment.NewLine)
                      .Append("and  pcr.rma_stat_id <> '1330'                                        ").Append(Environment.NewLine)
                      .Append(")                                                                     ").Append(Environment.NewLine)
                      .Append("GROUP BY ORIG_ORD                                                     ").Append(Environment.NewLine)
                      .Append(", ORIG_ORD_DTE                                                        ").Append(Environment.NewLine)
                      .Append(", CUST_NAME                                                           ").Append(Environment.NewLine)
                      .Append(", EMAIL                                                               ").Append(Environment.NewLine)
                      .Append(", PHONE                                                               ").Append(Environment.NewLine)
                      .Append(",  RMA_NUM                                                            ").Append(Environment.NewLine)
                      .Append(", RMA_CRTE_DTE                                                        ").Append(Environment.NewLine)
                      .Append(", RMA_STAT_NAME)                                                      ").Append(Environment.NewLine)
                      .Append(",                                                                     ").Append(Environment.NewLine)
                      .Append("NAME_COUNT AS (                                                       ").Append(Environment.NewLine)
                      .Append("              SELECT COUNT(CUST_NAME) CN                              ").Append(Environment.NewLine)
                      .Append("              , CUST_NAME NCCN                                        ").Append(Environment.NewLine)
                      .Append("              from BASE_DATA                                          ").Append(Environment.NewLine)
                      .Append("              GROUP BY CUST_NAME                                      ").Append(Environment.NewLine)
                      .Append("              )                                                       ").Append(Environment.NewLine)
                      .Append(",                                                                     ").Append(Environment.NewLine)
                      .Append("EMAIL_COUNT AS (                                                      ").Append(Environment.NewLine)
                      .Append("                select count(EMAIL) EC                                ").Append(Environment.NewLine)
                      .Append("                , EMAIL ECEM                                          ").Append(Environment.NewLine)
                      .Append("                from BASE_DATA                                        ").Append(Environment.NewLine)
                      .Append("                group by EMAIL                                        ").Append(Environment.NewLine)
                      .Append("                )                                                     ").Append(Environment.NewLine)
                      .Append(",                                                                     ").Append(Environment.NewLine)
                      .Append("PHONE_COUNT AS (                                                      ").Append(Environment.NewLine)
                      .Append("                select count(PHONE) PH                                ").Append(Environment.NewLine)
                      .Append("                , PHONE PCPH                                          ").Append(Environment.NewLine)
                      .Append("                from BASE_DATA                                        ").Append(Environment.NewLine)
                      .Append("                group by PHONE                                        ").Append(Environment.NewLine)
                      .Append("                )                                                     ").Append(Environment.NewLine)
                      .Append("select  BD.ORIG_ORD                                                   ").Append(Environment.NewLine)
                      .Append(",BD.ORIG_ORD_DTE                                                      ").Append(Environment.NewLine)
                      .Append(",BD.CUST_NAME                                                         ").Append(Environment.NewLine)
                      .Append(",BD.EMAIL                                                             ").Append(Environment.NewLine)
                      .Append(",BD.PHONE                                                             ").Append(Environment.NewLine)
                      .Append(",BD.RMA_NUM                                                           ").Append(Environment.NewLine)
                      .Append(",BD.RMA_CRTE_DTE                                                      ").Append(Environment.NewLine)
                      .Append(",BD.RMA_STAT_NAME                                                     ").Append(Environment.NewLine)
                      .Append(", NC.CN NAME_CT                                                       ").Append(Environment.NewLine)
                      .Append(", EC.EC EMAIL_CT                                                      ").Append(Environment.NewLine)
                      .Append(", PC.PH PHONE_CT                                                      ").Append(Environment.NewLine)
                      .Append("from BASE_DATA BD                                                     ").Append(Environment.NewLine)
                      .Append("join NAME_COUNT NC                                                    ").Append(Environment.NewLine)
                      .Append("on NC.NCCN = BD.CUST_NAME                                             ").Append(Environment.NewLine)
                      .Append("join EMAIL_COUNT EC                                                   ").Append(Environment.NewLine)
                      .Append("on EC.ECEM = BD.EMAIL                                                 ").Append(Environment.NewLine)
                      .Append("join PHONE_COUNT PC                                                   ").Append(Environment.NewLine)
                      .Append("on PC.PCPH = BD.PHONE                                                 ").Append(Environment.NewLine)
                      .Append("order by BD.RMA_CRTE_DTE desc                                         ").Append(Environment.NewLine);




                    DataTable RmaDnr = EOM.SQLToDataTable(sbCustInfo.ToString());

                    EOM.Open();


                    foreach (DataRow info in RmaDnr.Rows)
                    {
                        DataRow newResolution = resolutions.NewRow();
                        newResolution["OrdNum"] = info["ORIG_ORD"].ToString();
                        newResolution["OrdDte"] = info["ORIG_ORD_DTE"].ToString();
                        newResolution["CustName"] = info["CUST_NAME"].ToString();
                        newResolution["CustEmail"] = info["EMAIL"].ToString();
                        newResolution["CustPhone"] = info["PHONE"].ToString();
                        newResolution["RmaNum"] = info["RMA_NUM"].ToString();
                        newResolution["RmaDte"] = info["RMA_CRTE_DTE"].ToString();
                        newResolution["RmaStat"] = info["RMA_STAT_NAME"].ToString();
                        newResolution["NameCt"] = info["NAME_CT"].ToString();
                        newResolution["EmailCt"] = info["EMAIL_CT"].ToString();
                        newResolution["PhoneCt"] = info["PHONE_CT"].ToString();
                        resolutions.Rows.Add(newResolution);
                        continue;




                    }
                    EOM.Close();


                }

                //Create an Excel workbook and load the resolutions datatable into it to create the report.
                XLWorkbook report = new XLWorkbook();
                report.AddWorksheet(resolutions, "RmaDnr");

                using (MemoryStream mailAttachment = new MemoryStream())
                {
                    report.SaveAs(mailAttachment);
                    mailAttachment.Position = 0;
                    //Email the report.
                    Email.SendMail(ConfigurationManager.AppSettings["MailSender"], ConfigurationManager.AppSettings["MailRecipient"], "RMA DNR Report", "RMA DNR Report", new List<Attachment> { new Attachment(mailAttachment, "RmaDnr" + DateTime.Now.ToString("MMMM dd yyyy") + ".xlsx") });
                }


            }
        }
    }
}
