﻿using System.Net.Mail;
using System.Collections.Generic;

namespace RMA_DNR
{
    /// <summary>
    /// Sends email from the FinishLine exchange server
    /// </summary>
    static class Email
    {
        /// <summary>
        /// Send an email
        /// </summary>
        /// <param name="Sender">The address of the sender</param>
        /// <param name="Recipient">The email address of the recipient</param>
        /// <param name="Subject">The subject of the email</param>
        /// <param name="Message">The body of the email</param>
        /// <param name="Attachments">Optional attachment(s)</param>
        public static void SendMail(string Sender, string Recipient, string Subject, string Message, List<Attachment> Attachments = null)
        {
            SendMail(Sender, new List<string>() { Recipient }, Subject, Message, Attachments);
        }

        /// <summary>
        /// Send an email
        /// </summary>
        /// <param name="Sender">The address of the sender</param>
        /// <param name="Recipients">A list of email addresses of the recipients</param>
        /// <param name="Subject">The subject of the email</param>
        /// <param name="Message">The body of the email</param>
        /// <param name="Attachments">Optional attachment(s)</param>
        public static void SendMail(string Sender, List<string> Recipients, string Subject, string Message, List<Attachment> Attachments = null)
        {
            try
            {
                using (MailMessage email = new MailMessage())
                {
                    email.From = new MailAddress(Sender);
                    email.Subject = Subject;
                    email.Body = Message;
                    Recipients.ForEach(r => email.To.Add(new MailAddress(r)));
                    if (Attachments != null) { Attachments.ForEach(a => email.Attachments.Add(a)); }
                    if (Message.ToUpper().Contains("</HTML>")) { email.IsBodyHtml = true; }

                    using (SmtpClient mailServer = new SmtpClient())
                    {
                        mailServer.Host = "smtp.finishline.com";
                        mailServer.EnableSsl = false;
                        mailServer.Send(email);
                    }
                }
            }
            catch { throw; }
        }
    }
}
